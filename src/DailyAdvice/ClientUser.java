package DailyAdvice;

public class ClientUser extends DailyAdviceClient{
	private String userName;
	
	public ClientUser(String userName) {
		this.userName = userName;
	}
	
	void userRequest() {
		System.out.println("Please give me a daily advice!");

	}
	
	public void start() {
		System.out.println(userName + ": ");
		super.start();
	}
	
	public static void main(String[] args) {
		ClientUser user = new ClientUser("Jim");
		user.userRequest();
		user.start();	
	}

}
