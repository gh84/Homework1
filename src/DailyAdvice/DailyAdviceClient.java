package DailyAdvice;
import java.io.*;
import java.net.*;

public class DailyAdviceClient {
	
	private Socket s;

	public void start() {
		try {
			s = new Socket("127.0.0.1", 5000);
			InputStreamReader streamReader = new InputStreamReader(s.getInputStream());
			BufferedReader reader = new BufferedReader(streamReader);
			String advice = reader.readLine();
			System.out.println("Today you should: " + advice);
			reader.close();
		} 
		catch(IOException ex) {
			ex.printStackTrace();
		}
	}


}