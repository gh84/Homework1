package DailyAdvice;
import java.io.*;
import java.net.*;

public class DailyAdviceServer {
	
	String[] advices= {"Eat slowly","keep reading", "play the piano", "swim for an hour", "Don't foget to eat vegetable", "eating fruit is important"};
	private ServerSocket server;
	
	public void start() {
		try {
			server = new ServerSocket(5000);
			while(true) {
				Socket socket = server.accept();
				PrintWriter writer = new PrintWriter(socket.getOutputStream());
				String advice = getAdvice();
				writer.println(advice);
				writer.close();
				//System.out.println(advice);
			}
		} 
		catch(IOException ex) {
				ex.printStackTrace();
		}
	}
	
	private String getAdvice() {
		int random  = (int) (Math.random() * (advices.length));
		return advices[random];
	}
	
	public static void main(String[] args) {
		DailyAdviceServer server = new DailyAdviceServer();
		server.start();
		
	}
}
